def part_2():
    with open('aoc_input_1_1.txt') as f:
        contents = f.read().splitlines()

    count = 0

    for x in range(2, len(contents)):
        sum = int(contents[x]) + int(contents[x-1]) + int(contents[x-2])
        if sum > sum_prev:
            count += 1
        sum_prev = sum
    print(count)

part_2()
