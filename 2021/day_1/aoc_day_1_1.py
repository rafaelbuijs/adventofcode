def part_1():
    with open('aoc_input_1_1.txt') as f:
        contents = f.read().splitlines()
        
    count = 0

    for x in range(1, len(contents)):
        if int(contents[x]) > int(contents[x-1]):
            count += 1
    print(count)

part_1()