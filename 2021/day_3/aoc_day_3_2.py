def part_2():
    with open('aoc_input_3_1.txt') as f:
        contents = f.read().splitlines()
        
    g_1 = 0
    g_0 = 0
    b = 0
    contents_2 = contents

    for x in (range(0, len(contents))):
        for y in contents:
            if y[x] == "0":
                g_1 += 1
            else: 
                g_0 += 1
            if g_0 >= g_1:
                b = 0
            else:
                b = 1
        g_1 = 0
        g_0 = 0
        contents_copy = []
        for y in contents:
            if int(y[x]) == b:
                contents_copy.append(y)
            contents = contents_copy
        if len(contents) == 1:
            break
    csr = contents[0]

    for x in (range(0, len(contents_2))):
        for y in contents_2:
            if y[x] == "1":
                g_1 += 1
            else: 
                g_0 += 1
            if g_0 > g_1:
                b = 0
            else:
                b = 1
        g_1 = 0
        g_0 = 0
        contents_2_copy = []
        for y in contents_2:
            if int(y[x]) == b:
                contents_2_copy.append(y)
            contents_2 = contents_2_copy
        if len(contents_2) == 1:
            break
    ogr = contents_2[0]
    print(int(ogr,2)*(int(csr,2)))

part_2()