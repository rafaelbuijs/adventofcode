def part_1():
    with open('aoc_input_3_1.txt') as f:
        contents = f.read().splitlines()
        
    g_dict_bit = {}
    e_dict_bit = {}

    for x in range(0, len(contents[0])):
        g_dict_bit[x] = 0
        e_dict_bit[x] = 0
        for y in contents:
            if y[x] == "1":
                g_dict_bit[x] += 1

    for key, value in g_dict_bit.items():
        if int(value) > (len(contents)/2):
            g_dict_bit[key] = str(1)
            e_dict_bit[key] = str(0)
        else: 
            g_dict_bit[key] = str(0)
            e_dict_bit[key] = str(1)
    
    gamma = "".join(e_dict_bit.values())
    epsilon = "".join(g_dict_bit.values())
    print(int(gamma,2)*int(epsilon,2))
        

part_1()