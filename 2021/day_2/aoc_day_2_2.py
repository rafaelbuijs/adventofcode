import re

def part_2():
    with open('aoc_input_2_1.txt') as f:
        contents = f.read().splitlines()
        
    hor = 0
    dep = 0
    aim = 0

    for x in contents:
        if bool(re.search("forward", x)) == 1:
            hor += int(x[-1])
            dep += aim * int(x[-1])
        if bool(re.search('down', x)) == 1:
            aim += int(x[-1])
        if bool(re.search('up', x)) == 1:
            aim -= int(x[-1])

    print(hor*dep)

part_2()