import re

def part_1():
    with open('aoc_input_2_1.txt') as f:
        contents = f.read().splitlines()
        
    hor = 0
    dep = 0

    for x in contents:
        if bool(re.search('\bforward\b', x)) == 1:
            hor += int(x[-1])
        if bool(re.search('\bdown\b', x)) == 1:
            dep += int(x[-1])
        if bool(re.search('\bup\b', x)) == 1:
            dep -= int(x[-1])

    print(hor*dep)

part_1()